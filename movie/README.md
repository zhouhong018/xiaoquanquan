#xiaoquanquan
#{
	周宏昌：mongodb数据导入
	王学友：详情页+评论回复+后台
	徐浩林：详情页+评论回复+后台
	李伟波：git代码管理+注册登录codes(routers)
	胡玉红：list   html(views)+codes(routers)
	汪  杰/朱斌斌: header.ejs、footer.ejs、index.ejs、register.ejs、login.ejs
    朱斌斌：经理写文稿报告
}
1.接口部分
https://api.douban.com/v2/movie/in_theaters （正在热映）
https://api.douban.com/v2/movie/coming_soon (即将上映)
...

2.梳理功能模块
1.首页(菜单、轮询、分栏、注册、登录、底部版权)      http://localhost:3000/
2.注册                                           http://localhost:3000/register
3.登录                                           http://localhost:3000/login
4.视频列表页（分页）                              http://localhost:3000/movie/list?pageNo=1(标题、内容等查询)
5.视频详情页（评论内容--》登录用户才能进行评论，编辑器操作，上传操作，实现评论列表的分页） http://localhost:3000/movie/detail?id=123&pageNo=1
http://localhost:3000/data/init
moive_top250
coming_soon
....

router:                                        
    index.js(index,register,login)
    user(registerAction,loginAction)
    movie(list,detail,comment,upload)
    data(insert--)
views
    index.ejs
    register.ejs
    login.ejs
    list.ejs
    detail.ejs
    header.ejs
    footer.ejs

技术分析：
1.nodejs
2.express
3.mongodb
4.bootstrap
5.session
6.multiparty
....

环境模块：
cnpm i express-session  mongodb  -D
cnpm i async -S


遇到问题：
详情页+评论回复：1、没法通过id进入不同的详情页 2、评论的翻页功能没有实现 3、评论无法导入数据库 无法提取并渲染到ejs
列表页：搜索栏不会做 分页功能未实现
登录注册页：1、登录后跳转到user下面的loginactive之后无法跳转到index 2、密码存入数据库后为null 