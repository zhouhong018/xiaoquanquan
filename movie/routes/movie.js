var express = require('express');
var router = express.Router();
var async = require('async');
var MongoClient = require('mongodb').MongoClient;
var DB_CONN_STR = "mongodb://localhost:27017/movie";


router.get('/detail', function(req, res, next) {
    MongoClient.connect(DB_CONN_STR, function (err, db) {
        if (err) {
        return;
        } else {
        var conn = db.collection('movie_detail');
        var data = {id:req.query.id};
        console.log(data);
        conn.find(data).toArray(function(err,results){
          res.render('comment',results[0])
        })
        }
    })
});

module.exports = router;
