var express = require('express');
var router = express.Router();
var async = require('async');
var MongoClient = require('mongodb').MongoClient;
var DB_CONN_STR = "mongodb://localhost:27017/movie";
var multiparty = require('multiparty');
var fs = require('fs');
/* GET home page. */





router.get('/', function(req, res, next) {
  async.parallel([
       function(cb){
         // 轮播图的查询
         MongoClient.connect(DB_CONN_STR, function (err, db) {
          if (err) {
            return;
          } else {
            var conn = db.collection('top250');
            var data = {}
            conn.find(data).limit(5).skip(10).toArray(function(err,results){
              cb(null,results);
            })
          }
        })
       },
       function(cb){
         // top250
         MongoClient.connect(DB_CONN_STR, function (err, db) {
          if (err) {
            return;
          } else {
            var conn = db.collection('top250');
            var data = {}
            conn.find(data).limit(3).skip(5).toArray(function(err,results){
              cb(null,results);
            })
          }
        })
       },
       function(cb){
         // in_theaters
         MongoClient.connect(DB_CONN_STR, function (err, db) {
          if (err) {
            return;
          } else {
            var conn = db.collection('in_theaters');
            var data = {}
            conn.find(data).limit(3).toArray(function(err,results){
              cb(null,results);
            })
          }
        })
       },
       function(cb){
         // movie_coming_soon
         MongoClient.connect(DB_CONN_STR, function (err, db) {
          if (err) {
            return;
          } else {
            var conn = db.collection('coming_soon');
            var data = {}
            conn.find(data).limit(3).toArray(function(err,results){
              cb(null,results);
            })
          }
        })
       },
        function(cb){
         // 全网首播
         MongoClient.connect(DB_CONN_STR, function (err, db) {
          if (err) {
            return;
          } else {
            var conn = db.collection('coming_soon');
            var data = {}
            conn.find(data).limit(3).skip(10).toArray(function(err,results){
              cb(null,results);
            })
          }
        })
       },
        function(cb){
         // 猜你喜欢
         MongoClient.connect(DB_CONN_STR, function (err, db) {
          if (err) {
            return;
          } else {
            var conn = db.collection('coming_soon');
            var data = {}
            conn.find(data).limit(3).skip(15).toArray(function(err,results){
              cb(null,results);
            })
          }
        })
       }
     ],function(err,results){
       
         res.render('index', {
          carousel:results[0],
          top250: results[1],
          comingSoon: results[3],
          in_theaters:results[2],
          netfirst:results[4],
          guess:results[5]
        } );
     })

 
});
router.post('/uploadImg',function(req,res,next){

  var form = new multiparty.Form();
  form.encoding = 'utf-8';
  form.uploadDir = './uploadtmp';
  form.maxFilesSize = 2*1024*1024;
  form.parse(req,function(err,fields,files){
  // 就是取files对象里面的一些信息内容而已
  // 设置了一个目录上传的新的文件名结构以及路径地址
    var uploadurl = '/images/upload/';//移动地址
    var file = files['filedata'];//files下的对象
    var originalFilename = file[0].originalFilename;//原来的名字
    var tmpPath = file[0].path;//原始路径
    var timestamp = new Date().getTime();//唯一的文件名，弄个时间戳
    uploadurl += timestamp + originalFilename;//
    var newPath = './public' + uploadurl;
    var fileReadStream = fs.createReadStream(tmpPath);// 对原文件内容进行一个流文件信息的读取
    var fileWriteStream = fs.createWriteStream(newPath);
    fileReadStream.pipe(fileWriteStream);//管道操作，从读到写的过程
    fileWriteStream.on('close',function(){//传完之后的操作
    fs.unlinkSync(tmpPath);//删除原文件
    res.send('{"err":"","msg":"'+ uploadurl +'"}')//返回一个他要的格式的字符串
   })
  })
  

})
module.exports = router;
